﻿namespace PidProjesi_a1
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.textBoxDikeyHedef = new System.Windows.Forms.TextBox();
            this.labelDikeyHedef = new System.Windows.Forms.Label();
            this.buttonBaglan = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelDikeyKontrol = new System.Windows.Forms.Label();
            this.textBoxDikeyKontroL = new System.Windows.Forms.TextBox();
            this.labelDikeySensor = new System.Windows.Forms.Label();
            this.textBoxDikeySensor = new System.Windows.Forms.TextBox();
            this.labelYataySensor = new System.Windows.Forms.Label();
            this.textBoxYataySensor = new System.Windows.Forms.TextBox();
            this.labelYatayKontrol = new System.Windows.Forms.Label();
            this.textBoxYatayKontroL = new System.Windows.Forms.TextBox();
            this.labelYatayHedef = new System.Windows.Forms.Label();
            this.textBoxYatayHedef = new System.Windows.Forms.TextBox();
            this.buttonBaglantiKes = new System.Windows.Forms.Button();
            this.labelPortSecim = new System.Windows.Forms.Label();
            this.comboBoxPort = new System.Windows.Forms.ComboBox();
            this.comboBoxBaudrate = new System.Windows.Forms.ComboBox();
            this.labelBaudrateSecim = new System.Windows.Forms.Label();
            this.labelDikeyBaslik = new System.Windows.Forms.Label();
            this.labelYatayBaslik = new System.Windows.Forms.Label();
            this.buttonPidBasla = new System.Windows.Forms.Button();
            this.buttonPidDur = new System.Windows.Forms.Button();
            this.labelPidDurumBaslik = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.checkBoxDenetim = new System.Windows.Forms.CheckBox();
            this.buttonDenetimButonu = new System.Windows.Forms.Button();
            this.groupBoxKatsayiAyari = new System.Windows.Forms.GroupBox();
            this.textBoxYataySet = new System.Windows.Forms.TextBox();
            this.textBoxYatayKd = new System.Windows.Forms.TextBox();
            this.textBoxYatayKi = new System.Windows.Forms.TextBox();
            this.textBoxYatayKp = new System.Windows.Forms.TextBox();
            this.textBoxDikeySet = new System.Windows.Forms.TextBox();
            this.textBoxDikeyKd = new System.Windows.Forms.TextBox();
            this.textBoxDikeyKi = new System.Windows.Forms.TextBox();
            this.textBoxDikeyKp = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.trackBarDikeyKp = new System.Windows.Forms.TrackBar();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.LabelPidDurumu = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.trackBarDikeyKi = new System.Windows.Forms.TrackBar();
            this.trackBarDikeyKd = new System.Windows.Forms.TrackBar();
            this.trackBarDikeySet = new System.Windows.Forms.TrackBar();
            this.trackBarYatayKp = new System.Windows.Forms.TrackBar();
            this.trackBarYatayKi = new System.Windows.Forms.TrackBar();
            this.trackBarYatayKd = new System.Windows.Forms.TrackBar();
            this.trackBarYataySet = new System.Windows.Forms.TrackBar();
            this.buttonPrePidDurumu = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.groupBoxKatsayiAyari.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDikeyKp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDikeyKi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDikeyKd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDikeySet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYatayKp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYatayKi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYatayKd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYataySet)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxDikeyHedef
            // 
            this.textBoxDikeyHedef.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxDikeyHedef.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxDikeyHedef.ForeColor = System.Drawing.Color.Green;
            this.textBoxDikeyHedef.Location = new System.Drawing.Point(207, 291);
            this.textBoxDikeyHedef.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDikeyHedef.Name = "textBoxDikeyHedef";
            this.textBoxDikeyHedef.Size = new System.Drawing.Size(54, 27);
            this.textBoxDikeyHedef.TabIndex = 0;
            // 
            // labelDikeyHedef
            // 
            this.labelDikeyHedef.AutoSize = true;
            this.labelDikeyHedef.Location = new System.Drawing.Point(30, 295);
            this.labelDikeyHedef.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDikeyHedef.Name = "labelDikeyHedef";
            this.labelDikeyHedef.Size = new System.Drawing.Size(110, 16);
            this.labelDikeyHedef.TabIndex = 1;
            this.labelDikeyHedef.Text = "Dikey Açı Hedefi:";
            // 
            // buttonBaglan
            // 
            this.buttonBaglan.ForeColor = System.Drawing.Color.Green;
            this.buttonBaglan.Location = new System.Drawing.Point(20, 199);
            this.buttonBaglan.Margin = new System.Windows.Forms.Padding(4);
            this.buttonBaglan.Name = "buttonBaglan";
            this.buttonBaglan.Size = new System.Drawing.Size(109, 33);
            this.buttonBaglan.TabIndex = 2;
            this.buttonBaglan.Text = "Bağlan";
            this.buttonBaglan.UseVisualStyleBackColor = true;
            this.buttonBaglan.Click += new System.EventHandler(this.buttonBaglan_Click);
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(293, 15);
            this.chart1.Margin = new System.Windows.Forms.Padding(4);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Legend = "Legend1";
            series1.Name = "DikeyKontrolSinyali";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Legend = "Legend1";
            series2.Name = "DikeySensor";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Legend = "Legend1";
            series3.Name = "DikeyHedef";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(511, 285);
            this.chart1.TabIndex = 3;
            this.chart1.Text = "chart1";
            // 
            // chart2
            // 
            chartArea2.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart2.Legends.Add(legend2);
            this.chart2.Location = new System.Drawing.Point(293, 308);
            this.chart2.Margin = new System.Windows.Forms.Padding(4);
            this.chart2.Name = "chart2";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series4.Legend = "Legend1";
            series4.Name = "YatayKontrolSinyali";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series5.Legend = "Legend1";
            series5.Name = "YataySensor";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series6.Legend = "Legend1";
            series6.Name = "YatayHedef";
            this.chart2.Series.Add(series4);
            this.chart2.Series.Add(series5);
            this.chart2.Series.Add(series6);
            this.chart2.Size = new System.Drawing.Size(511, 285);
            this.chart2.TabIndex = 4;
            this.chart2.Text = "chart2";
            // 
            // labelDikeyKontrol
            // 
            this.labelDikeyKontrol.AutoSize = true;
            this.labelDikeyKontrol.Location = new System.Drawing.Point(30, 327);
            this.labelDikeyKontrol.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDikeyKontrol.Name = "labelDikeyKontrol";
            this.labelDikeyKontrol.Size = new System.Drawing.Size(154, 16);
            this.labelDikeyKontrol.TabIndex = 6;
            this.labelDikeyKontrol.Text = "Dikey Açı Kontrol Sinyali:";
            // 
            // textBoxDikeyKontroL
            // 
            this.textBoxDikeyKontroL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxDikeyKontroL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxDikeyKontroL.ForeColor = System.Drawing.Color.Green;
            this.textBoxDikeyKontroL.Location = new System.Drawing.Point(207, 323);
            this.textBoxDikeyKontroL.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDikeyKontroL.Name = "textBoxDikeyKontroL";
            this.textBoxDikeyKontroL.Size = new System.Drawing.Size(54, 27);
            this.textBoxDikeyKontroL.TabIndex = 5;
            // 
            // labelDikeySensor
            // 
            this.labelDikeySensor.AutoSize = true;
            this.labelDikeySensor.Location = new System.Drawing.Point(30, 359);
            this.labelDikeySensor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDikeySensor.Name = "labelDikeySensor";
            this.labelDikeySensor.Size = new System.Drawing.Size(157, 16);
            this.labelDikeySensor.TabIndex = 8;
            this.labelDikeySensor.Text = "Dikey Açı Sensör Değeri:";
            // 
            // textBoxDikeySensor
            // 
            this.textBoxDikeySensor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxDikeySensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxDikeySensor.ForeColor = System.Drawing.Color.Green;
            this.textBoxDikeySensor.Location = new System.Drawing.Point(207, 355);
            this.textBoxDikeySensor.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDikeySensor.Name = "textBoxDikeySensor";
            this.textBoxDikeySensor.Size = new System.Drawing.Size(54, 27);
            this.textBoxDikeySensor.TabIndex = 7;
            // 
            // labelYataySensor
            // 
            this.labelYataySensor.AutoSize = true;
            this.labelYataySensor.Location = new System.Drawing.Point(30, 501);
            this.labelYataySensor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelYataySensor.Name = "labelYataySensor";
            this.labelYataySensor.Size = new System.Drawing.Size(157, 16);
            this.labelYataySensor.TabIndex = 14;
            this.labelYataySensor.Text = "Yatay Açı Sensör Değeri:";
            // 
            // textBoxYataySensor
            // 
            this.textBoxYataySensor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxYataySensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxYataySensor.ForeColor = System.Drawing.Color.Green;
            this.textBoxYataySensor.Location = new System.Drawing.Point(207, 498);
            this.textBoxYataySensor.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxYataySensor.Name = "textBoxYataySensor";
            this.textBoxYataySensor.Size = new System.Drawing.Size(54, 27);
            this.textBoxYataySensor.TabIndex = 13;
            // 
            // labelYatayKontrol
            // 
            this.labelYatayKontrol.AutoSize = true;
            this.labelYatayKontrol.Location = new System.Drawing.Point(30, 469);
            this.labelYatayKontrol.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelYatayKontrol.Name = "labelYatayKontrol";
            this.labelYatayKontrol.Size = new System.Drawing.Size(154, 16);
            this.labelYatayKontrol.TabIndex = 12;
            this.labelYatayKontrol.Text = "Yatay Açı Kontrol Sinyali:";
            // 
            // textBoxYatayKontroL
            // 
            this.textBoxYatayKontroL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxYatayKontroL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxYatayKontroL.ForeColor = System.Drawing.Color.Green;
            this.textBoxYatayKontroL.Location = new System.Drawing.Point(207, 466);
            this.textBoxYatayKontroL.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxYatayKontroL.Name = "textBoxYatayKontroL";
            this.textBoxYatayKontroL.Size = new System.Drawing.Size(54, 27);
            this.textBoxYatayKontroL.TabIndex = 11;
            // 
            // labelYatayHedef
            // 
            this.labelYatayHedef.AutoSize = true;
            this.labelYatayHedef.Location = new System.Drawing.Point(30, 437);
            this.labelYatayHedef.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelYatayHedef.Name = "labelYatayHedef";
            this.labelYatayHedef.Size = new System.Drawing.Size(110, 16);
            this.labelYatayHedef.TabIndex = 10;
            this.labelYatayHedef.Text = "Yatay Açı Hedefi:";
            // 
            // textBoxYatayHedef
            // 
            this.textBoxYatayHedef.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxYatayHedef.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxYatayHedef.ForeColor = System.Drawing.Color.Green;
            this.textBoxYatayHedef.Location = new System.Drawing.Point(207, 434);
            this.textBoxYatayHedef.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxYatayHedef.Name = "textBoxYatayHedef";
            this.textBoxYatayHedef.Size = new System.Drawing.Size(54, 27);
            this.textBoxYatayHedef.TabIndex = 9;
            // 
            // buttonBaglantiKes
            // 
            this.buttonBaglantiKes.ForeColor = System.Drawing.Color.Red;
            this.buttonBaglantiKes.Location = new System.Drawing.Point(157, 199);
            this.buttonBaglantiKes.Margin = new System.Windows.Forms.Padding(4);
            this.buttonBaglantiKes.Name = "buttonBaglantiKes";
            this.buttonBaglantiKes.Size = new System.Drawing.Size(109, 33);
            this.buttonBaglantiKes.TabIndex = 15;
            this.buttonBaglantiKes.Text = "Bağlantiyi Kes";
            this.buttonBaglantiKes.UseVisualStyleBackColor = true;
            this.buttonBaglantiKes.Click += new System.EventHandler(this.buttonBaglantiKes_Click);
            // 
            // labelPortSecim
            // 
            this.labelPortSecim.AutoSize = true;
            this.labelPortSecim.Location = new System.Drawing.Point(15, 119);
            this.labelPortSecim.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPortSecim.Name = "labelPortSecim";
            this.labelPortSecim.Size = new System.Drawing.Size(78, 16);
            this.labelPortSecim.TabIndex = 16;
            this.labelPortSecim.Text = "Port Seçimi:";
            // 
            // comboBoxPort
            // 
            this.comboBoxPort.FormattingEnabled = true;
            this.comboBoxPort.Location = new System.Drawing.Point(19, 139);
            this.comboBoxPort.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxPort.Name = "comboBoxPort";
            this.comboBoxPort.Size = new System.Drawing.Size(109, 24);
            this.comboBoxPort.TabIndex = 17;
            // 
            // comboBoxBaudrate
            // 
            this.comboBoxBaudrate.FormattingEnabled = true;
            this.comboBoxBaudrate.Location = new System.Drawing.Point(157, 139);
            this.comboBoxBaudrate.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxBaudrate.Name = "comboBoxBaudrate";
            this.comboBoxBaudrate.Size = new System.Drawing.Size(109, 24);
            this.comboBoxBaudrate.TabIndex = 18;
            // 
            // labelBaudrateSecim
            // 
            this.labelBaudrateSecim.AutoSize = true;
            this.labelBaudrateSecim.Location = new System.Drawing.Point(153, 119);
            this.labelBaudrateSecim.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelBaudrateSecim.Name = "labelBaudrateSecim";
            this.labelBaudrateSecim.Size = new System.Drawing.Size(109, 16);
            this.labelBaudrateSecim.TabIndex = 19;
            this.labelBaudrateSecim.Text = "Baudrate Seçimi:";
            // 
            // labelDikeyBaslik
            // 
            this.labelDikeyBaslik.AutoSize = true;
            this.labelDikeyBaslik.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelDikeyBaslik.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelDikeyBaslik.Location = new System.Drawing.Point(54, 257);
            this.labelDikeyBaslik.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDikeyBaslik.Name = "labelDikeyBaslik";
            this.labelDikeyBaslik.Size = new System.Drawing.Size(172, 25);
            this.labelDikeyBaslik.TabIndex = 20;
            this.labelDikeyBaslik.Text = "Dikey Açı Kontrolü";
            // 
            // labelYatayBaslik
            // 
            this.labelYatayBaslik.AutoSize = true;
            this.labelYatayBaslik.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelYatayBaslik.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelYatayBaslik.Location = new System.Drawing.Point(53, 402);
            this.labelYatayBaslik.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelYatayBaslik.Name = "labelYatayBaslik";
            this.labelYatayBaslik.Size = new System.Drawing.Size(173, 25);
            this.labelYatayBaslik.TabIndex = 21;
            this.labelYatayBaslik.Text = "Yatay Açı Kontrolü";
            // 
            // buttonPidBasla
            // 
            this.buttonPidBasla.ForeColor = System.Drawing.Color.Green;
            this.buttonPidBasla.Location = new System.Drawing.Point(2, 533);
            this.buttonPidBasla.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPidBasla.Name = "buttonPidBasla";
            this.buttonPidBasla.Size = new System.Drawing.Size(143, 33);
            this.buttonPidBasla.TabIndex = 22;
            this.buttonPidBasla.Text = "Pid kontrolü başlat";
            this.buttonPidBasla.UseVisualStyleBackColor = true;
            this.buttonPidBasla.Click += new System.EventHandler(this.buttonPidBasla_Click);
            // 
            // buttonPidDur
            // 
            this.buttonPidDur.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonPidDur.Location = new System.Drawing.Point(147, 533);
            this.buttonPidDur.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPidDur.Name = "buttonPidDur";
            this.buttonPidDur.Size = new System.Drawing.Size(143, 33);
            this.buttonPidDur.TabIndex = 23;
            this.buttonPidDur.Text = "Pid kontrolü durdur";
            this.buttonPidDur.UseVisualStyleBackColor = true;
            this.buttonPidDur.Click += new System.EventHandler(this.buttonPidDur_Click);
            // 
            // labelPidDurumBaslik
            // 
            this.labelPidDurumBaslik.AutoSize = true;
            this.labelPidDurumBaslik.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelPidDurumBaslik.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelPidDurumBaslik.Location = new System.Drawing.Point(28, 29);
            this.labelPidDurumBaslik.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPidDurumBaslik.Name = "labelPidDurumBaslik";
            this.labelPidDurumBaslik.Size = new System.Drawing.Size(239, 50);
            this.labelPidDurumBaslik.TabIndex = 24;
            this.labelPidDurumBaslik.Text = "Mikrodenetleyici Kontrollü \r\n     Pid Denetim Arayüzü";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1821, 561);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 16);
            this.label9.TabIndex = 49;
            this.label9.Text = "160";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1821, 495);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 16);
            this.label10.TabIndex = 48;
            this.label10.Text = "15";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1821, 428);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 16);
            this.label11.TabIndex = 47;
            this.label11.Text = "15";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1821, 362);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 16);
            this.label12.TabIndex = 46;
            this.label12.Text = "15";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1821, 295);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 16);
            this.label13.TabIndex = 45;
            this.label13.Text = "160";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1821, 229);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 16);
            this.label14.TabIndex = 44;
            this.label14.Text = "15";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(1821, 162);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 16);
            this.label15.TabIndex = 43;
            this.label15.Text = "15";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(1821, 96);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 16);
            this.label16.TabIndex = 42;
            this.label16.Text = "15";
            // 
            // checkBoxDenetim
            // 
            this.checkBoxDenetim.AutoSize = true;
            this.checkBoxDenetim.Location = new System.Drawing.Point(1, 522);
            this.checkBoxDenetim.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxDenetim.Name = "checkBoxDenetim";
            this.checkBoxDenetim.Size = new System.Drawing.Size(153, 20);
            this.checkBoxDenetim.TabIndex = 69;
            this.checkBoxDenetim.Text = "HepsiniSenkron Yap";
            this.checkBoxDenetim.UseVisualStyleBackColor = true;
            this.checkBoxDenetim.CheckedChanged += new System.EventHandler(this.checkBoxDenetim_CheckedChanged);
            // 
            // buttonDenetimButonu
            // 
            this.buttonDenetimButonu.Location = new System.Drawing.Point(44, 542);
            this.buttonDenetimButonu.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDenetimButonu.Name = "buttonDenetimButonu";
            this.buttonDenetimButonu.Size = new System.Drawing.Size(161, 33);
            this.buttonDenetimButonu.TabIndex = 70;
            this.buttonDenetimButonu.Text = "Toplu Ayarla";
            this.buttonDenetimButonu.UseVisualStyleBackColor = true;
            this.buttonDenetimButonu.Click += new System.EventHandler(this.buttonDenetimButonu_Click);
            // 
            // groupBoxKatsayiAyari
            // 
            this.groupBoxKatsayiAyari.Controls.Add(this.buttonPrePidDurumu);
            this.groupBoxKatsayiAyari.Controls.Add(this.trackBarYataySet);
            this.groupBoxKatsayiAyari.Controls.Add(this.trackBarYatayKd);
            this.groupBoxKatsayiAyari.Controls.Add(this.trackBarYatayKi);
            this.groupBoxKatsayiAyari.Controls.Add(this.trackBarYatayKp);
            this.groupBoxKatsayiAyari.Controls.Add(this.trackBarDikeySet);
            this.groupBoxKatsayiAyari.Controls.Add(this.trackBarDikeyKd);
            this.groupBoxKatsayiAyari.Controls.Add(this.trackBarDikeyKi);
            this.groupBoxKatsayiAyari.Controls.Add(this.label1);
            this.groupBoxKatsayiAyari.Controls.Add(this.label2);
            this.groupBoxKatsayiAyari.Controls.Add(this.label3);
            this.groupBoxKatsayiAyari.Controls.Add(this.label5);
            this.groupBoxKatsayiAyari.Controls.Add(this.label6);
            this.groupBoxKatsayiAyari.Controls.Add(this.label7);
            this.groupBoxKatsayiAyari.Controls.Add(this.label8);
            this.groupBoxKatsayiAyari.Controls.Add(this.label33);
            this.groupBoxKatsayiAyari.Controls.Add(this.textBoxYataySet);
            this.groupBoxKatsayiAyari.Controls.Add(this.textBoxYatayKd);
            this.groupBoxKatsayiAyari.Controls.Add(this.textBoxYatayKi);
            this.groupBoxKatsayiAyari.Controls.Add(this.textBoxYatayKp);
            this.groupBoxKatsayiAyari.Controls.Add(this.textBoxDikeySet);
            this.groupBoxKatsayiAyari.Controls.Add(this.textBoxDikeyKd);
            this.groupBoxKatsayiAyari.Controls.Add(this.textBoxDikeyKi);
            this.groupBoxKatsayiAyari.Controls.Add(this.textBoxDikeyKp);
            this.groupBoxKatsayiAyari.Controls.Add(this.label4);
            this.groupBoxKatsayiAyari.Controls.Add(this.label17);
            this.groupBoxKatsayiAyari.Controls.Add(this.label18);
            this.groupBoxKatsayiAyari.Controls.Add(this.label27);
            this.groupBoxKatsayiAyari.Controls.Add(this.label19);
            this.groupBoxKatsayiAyari.Controls.Add(this.label28);
            this.groupBoxKatsayiAyari.Controls.Add(this.label20);
            this.groupBoxKatsayiAyari.Controls.Add(this.label29);
            this.groupBoxKatsayiAyari.Controls.Add(this.label21);
            this.groupBoxKatsayiAyari.Controls.Add(this.label30);
            this.groupBoxKatsayiAyari.Controls.Add(this.label22);
            this.groupBoxKatsayiAyari.Controls.Add(this.label31);
            this.groupBoxKatsayiAyari.Controls.Add(this.label32);
            this.groupBoxKatsayiAyari.Controls.Add(this.buttonDenetimButonu);
            this.groupBoxKatsayiAyari.Controls.Add(this.label23);
            this.groupBoxKatsayiAyari.Controls.Add(this.trackBarDikeyKp);
            this.groupBoxKatsayiAyari.Controls.Add(this.label24);
            this.groupBoxKatsayiAyari.Controls.Add(this.checkBoxDenetim);
            this.groupBoxKatsayiAyari.Controls.Add(this.label25);
            this.groupBoxKatsayiAyari.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.groupBoxKatsayiAyari.Location = new System.Drawing.Point(812, 11);
            this.groupBoxKatsayiAyari.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxKatsayiAyari.Name = "groupBoxKatsayiAyari";
            this.groupBoxKatsayiAyari.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxKatsayiAyari.Size = new System.Drawing.Size(413, 581);
            this.groupBoxKatsayiAyari.TabIndex = 71;
            this.groupBoxKatsayiAyari.TabStop = false;
            // 
            // textBoxYataySet
            // 
            this.textBoxYataySet.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxYataySet.ForeColor = System.Drawing.Color.Green;
            this.textBoxYataySet.Location = new System.Drawing.Point(74, 488);
            this.textBoxYataySet.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxYataySet.Name = "textBoxYataySet";
            this.textBoxYataySet.Size = new System.Drawing.Size(38, 27);
            this.textBoxYataySet.TabIndex = 86;
            // 
            // textBoxYatayKd
            // 
            this.textBoxYatayKd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxYatayKd.ForeColor = System.Drawing.Color.Green;
            this.textBoxYatayKd.Location = new System.Drawing.Point(74, 423);
            this.textBoxYatayKd.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxYatayKd.Name = "textBoxYatayKd";
            this.textBoxYatayKd.Size = new System.Drawing.Size(38, 27);
            this.textBoxYatayKd.TabIndex = 85;
            // 
            // textBoxYatayKi
            // 
            this.textBoxYatayKi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxYatayKi.ForeColor = System.Drawing.Color.Green;
            this.textBoxYatayKi.Location = new System.Drawing.Point(74, 357);
            this.textBoxYatayKi.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxYatayKi.Name = "textBoxYatayKi";
            this.textBoxYatayKi.Size = new System.Drawing.Size(38, 27);
            this.textBoxYatayKi.TabIndex = 84;
            // 
            // textBoxYatayKp
            // 
            this.textBoxYatayKp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxYatayKp.ForeColor = System.Drawing.Color.Green;
            this.textBoxYatayKp.Location = new System.Drawing.Point(74, 291);
            this.textBoxYatayKp.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxYatayKp.Name = "textBoxYatayKp";
            this.textBoxYatayKp.Size = new System.Drawing.Size(38, 27);
            this.textBoxYatayKp.TabIndex = 83;
            // 
            // textBoxDikeySet
            // 
            this.textBoxDikeySet.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxDikeySet.ForeColor = System.Drawing.Color.Green;
            this.textBoxDikeySet.Location = new System.Drawing.Point(74, 224);
            this.textBoxDikeySet.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDikeySet.Name = "textBoxDikeySet";
            this.textBoxDikeySet.Size = new System.Drawing.Size(38, 27);
            this.textBoxDikeySet.TabIndex = 82;
            // 
            // textBoxDikeyKd
            // 
            this.textBoxDikeyKd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxDikeyKd.ForeColor = System.Drawing.Color.Green;
            this.textBoxDikeyKd.Location = new System.Drawing.Point(74, 157);
            this.textBoxDikeyKd.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDikeyKd.Name = "textBoxDikeyKd";
            this.textBoxDikeyKd.Size = new System.Drawing.Size(38, 27);
            this.textBoxDikeyKd.TabIndex = 81;
            // 
            // textBoxDikeyKi
            // 
            this.textBoxDikeyKi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxDikeyKi.ForeColor = System.Drawing.Color.Green;
            this.textBoxDikeyKi.Location = new System.Drawing.Point(74, 91);
            this.textBoxDikeyKi.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDikeyKi.Name = "textBoxDikeyKi";
            this.textBoxDikeyKi.Size = new System.Drawing.Size(38, 27);
            this.textBoxDikeyKi.TabIndex = 80;
            // 
            // textBoxDikeyKp
            // 
            this.textBoxDikeyKp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxDikeyKp.ForeColor = System.Drawing.Color.Green;
            this.textBoxDikeyKp.Location = new System.Drawing.Point(74, 28);
            this.textBoxDikeyKp.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDikeyKp.Name = "textBoxDikeyKp";
            this.textBoxDikeyKp.Size = new System.Drawing.Size(38, 27);
            this.textBoxDikeyKp.TabIndex = 74;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(384, 91);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 16);
            this.label4.TabIndex = 72;
            this.label4.Text = "15";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(384, 354);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 16);
            this.label17.TabIndex = 79;
            this.label17.Text = "15";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(2, 491);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 16);
            this.label18.TabIndex = 79;
            this.label18.Text = "Yatay Set:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(384, 227);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(28, 16);
            this.label27.TabIndex = 78;
            this.label27.Text = "160";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(2, 425);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 16);
            this.label19.TabIndex = 78;
            this.label19.Text = "Yatay Kd:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(384, 25);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(21, 16);
            this.label28.TabIndex = 77;
            this.label28.Text = "15";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(2, 359);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 16);
            this.label20.TabIndex = 77;
            this.label20.Text = "Yatay Ki:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(384, 490);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(28, 16);
            this.label29.TabIndex = 76;
            this.label29.Text = "160";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(2, 292);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 16);
            this.label21.TabIndex = 76;
            this.label21.Text = "Yatay Kp:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(384, 424);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(21, 16);
            this.label30.TabIndex = 75;
            this.label30.Text = "15";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(2, 226);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 16);
            this.label22.TabIndex = 75;
            this.label22.Text = "Dikey Set:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(384, 291);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(21, 16);
            this.label31.TabIndex = 74;
            this.label31.Text = "15";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(384, 158);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(21, 16);
            this.label32.TabIndex = 73;
            this.label32.Text = "15";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(2, 159);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(64, 16);
            this.label23.TabIndex = 74;
            this.label23.Text = "Dikey Kd:";
            // 
            // trackBarDikeyKp
            // 
            this.trackBarDikeyKp.Location = new System.Drawing.Point(143, 24);
            this.trackBarDikeyKp.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarDikeyKp.Maximum = 15;
            this.trackBarDikeyKp.Name = "trackBarDikeyKp";
            this.trackBarDikeyKp.Size = new System.Drawing.Size(245, 56);
            this.trackBarDikeyKp.TabIndex = 72;
            this.trackBarDikeyKp.Scroll += new System.EventHandler(this.trackBarDikeyKp_Scroll);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(2, 93);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 16);
            this.label24.TabIndex = 73;
            this.label24.Text = "Dikey Ki:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(2, 31);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(64, 16);
            this.label25.TabIndex = 72;
            this.label25.Text = "Dikey Kp:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(87, 174);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(100, 16);
            this.label26.TabIndex = 72;
            this.label26.Text = "Veri Biçimi: 8N1";
            // 
            // LabelPidDurumu
            // 
            this.LabelPidDurumu.AutoSize = true;
            this.LabelPidDurumu.Location = new System.Drawing.Point(84, 572);
            this.LabelPidDurumu.Name = "LabelPidDurumu";
            this.LabelPidDurumu.Size = new System.Drawing.Size(76, 16);
            this.LabelPidDurumu.TabIndex = 73;
            this.LabelPidDurumu.Text = "Pid Durumu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(128, 93);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 16);
            this.label1.TabIndex = 87;
            this.label1.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(128, 356);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 16);
            this.label2.TabIndex = 94;
            this.label2.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(128, 229);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 16);
            this.label3.TabIndex = 93;
            this.label3.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(128, 27);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 16);
            this.label5.TabIndex = 92;
            this.label5.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(128, 492);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 16);
            this.label6.TabIndex = 91;
            this.label6.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(128, 426);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 16);
            this.label7.TabIndex = 90;
            this.label7.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(128, 293);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 16);
            this.label8.TabIndex = 89;
            this.label8.Text = "0";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(128, 160);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(14, 16);
            this.label33.TabIndex = 88;
            this.label33.Text = "0";
            // 
            // trackBarDikeyKi
            // 
            this.trackBarDikeyKi.Location = new System.Drawing.Point(143, 88);
            this.trackBarDikeyKi.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarDikeyKi.Maximum = 15;
            this.trackBarDikeyKi.Name = "trackBarDikeyKi";
            this.trackBarDikeyKi.Size = new System.Drawing.Size(245, 56);
            this.trackBarDikeyKi.TabIndex = 95;
            this.trackBarDikeyKi.Scroll += new System.EventHandler(this.trackBarDikeyKi_Scroll);
            // 
            // trackBarDikeyKd
            // 
            this.trackBarDikeyKd.Location = new System.Drawing.Point(143, 157);
            this.trackBarDikeyKd.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarDikeyKd.Maximum = 15;
            this.trackBarDikeyKd.Name = "trackBarDikeyKd";
            this.trackBarDikeyKd.Size = new System.Drawing.Size(245, 56);
            this.trackBarDikeyKd.TabIndex = 96;
            this.trackBarDikeyKd.Scroll += new System.EventHandler(this.trackBarDikeyKd_Scroll);
            // 
            // trackBarDikeySet
            // 
            this.trackBarDikeySet.Location = new System.Drawing.Point(143, 224);
            this.trackBarDikeySet.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarDikeySet.Maximum = 160;
            this.trackBarDikeySet.Name = "trackBarDikeySet";
            this.trackBarDikeySet.Size = new System.Drawing.Size(245, 56);
            this.trackBarDikeySet.TabIndex = 97;
            this.trackBarDikeySet.Scroll += new System.EventHandler(this.trackBarDikeySet_Scroll);
            // 
            // trackBarYatayKp
            // 
            this.trackBarYatayKp.Location = new System.Drawing.Point(143, 292);
            this.trackBarYatayKp.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarYatayKp.Maximum = 15;
            this.trackBarYatayKp.Name = "trackBarYatayKp";
            this.trackBarYatayKp.Size = new System.Drawing.Size(245, 56);
            this.trackBarYatayKp.TabIndex = 98;
            this.trackBarYatayKp.Scroll += new System.EventHandler(this.trackBarYatayKp_Scroll);
            // 
            // trackBarYatayKi
            // 
            this.trackBarYatayKi.Location = new System.Drawing.Point(143, 357);
            this.trackBarYatayKi.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarYatayKi.Maximum = 15;
            this.trackBarYatayKi.Name = "trackBarYatayKi";
            this.trackBarYatayKi.Size = new System.Drawing.Size(245, 56);
            this.trackBarYatayKi.TabIndex = 99;
            this.trackBarYatayKi.Scroll += new System.EventHandler(this.trackBarYatayKi_Scroll);
            // 
            // trackBarYatayKd
            // 
            this.trackBarYatayKd.Location = new System.Drawing.Point(143, 423);
            this.trackBarYatayKd.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarYatayKd.Maximum = 15;
            this.trackBarYatayKd.Name = "trackBarYatayKd";
            this.trackBarYatayKd.Size = new System.Drawing.Size(245, 56);
            this.trackBarYatayKd.TabIndex = 100;
            this.trackBarYatayKd.Scroll += new System.EventHandler(this.trackBarYatayKd_Scroll);
            // 
            // trackBarYataySet
            // 
            this.trackBarYataySet.Location = new System.Drawing.Point(146, 487);
            this.trackBarYataySet.Margin = new System.Windows.Forms.Padding(4);
            this.trackBarYataySet.Maximum = 160;
            this.trackBarYataySet.Name = "trackBarYataySet";
            this.trackBarYataySet.Size = new System.Drawing.Size(245, 56);
            this.trackBarYataySet.TabIndex = 101;
            this.trackBarYataySet.Scroll += new System.EventHandler(this.trackBarYataySet_Scroll);
            // 
            // buttonPrePidDurumu
            // 
            this.buttonPrePidDurumu.Location = new System.Drawing.Point(212, 542);
            this.buttonPrePidDurumu.Name = "buttonPrePidDurumu";
            this.buttonPrePidDurumu.Size = new System.Drawing.Size(162, 33);
            this.buttonPrePidDurumu.TabIndex = 102;
            this.buttonPrePidDurumu.Text = "Ayarla";
            this.buttonPrePidDurumu.UseVisualStyleBackColor = true;
            this.buttonPrePidDurumu.Click += new System.EventHandler(this.buttonPrePidDurumu_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1236, 596);
            this.Controls.Add(this.LabelPidDurumu);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.labelPidDurumBaslik);
            this.Controls.Add(this.buttonPidDur);
            this.Controls.Add(this.buttonPidBasla);
            this.Controls.Add(this.labelYatayBaslik);
            this.Controls.Add(this.labelDikeyBaslik);
            this.Controls.Add(this.labelBaudrateSecim);
            this.Controls.Add(this.comboBoxBaudrate);
            this.Controls.Add(this.comboBoxPort);
            this.Controls.Add(this.labelPortSecim);
            this.Controls.Add(this.buttonBaglantiKes);
            this.Controls.Add(this.labelYataySensor);
            this.Controls.Add(this.textBoxYataySensor);
            this.Controls.Add(this.labelYatayKontrol);
            this.Controls.Add(this.textBoxYatayKontroL);
            this.Controls.Add(this.labelYatayHedef);
            this.Controls.Add(this.textBoxYatayHedef);
            this.Controls.Add(this.labelDikeySensor);
            this.Controls.Add(this.textBoxDikeySensor);
            this.Controls.Add(this.labelDikeyKontrol);
            this.Controls.Add(this.textBoxDikeyKontroL);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.buttonBaglan);
            this.Controls.Add(this.labelDikeyHedef);
            this.Controls.Add(this.textBoxDikeyHedef);
            this.Controls.Add(this.groupBoxKatsayiAyari);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.groupBoxKatsayiAyari.ResumeLayout(false);
            this.groupBoxKatsayiAyari.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDikeyKp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDikeyKi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDikeyKd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDikeySet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYatayKp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYatayKi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYatayKd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYataySet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TextBox textBoxDikeyHedef;
        private System.Windows.Forms.Label labelDikeyHedef;
        private System.Windows.Forms.Button buttonBaglan;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Label labelDikeyKontrol;
        private System.Windows.Forms.TextBox textBoxDikeyKontroL;
        private System.Windows.Forms.Label labelDikeySensor;
        private System.Windows.Forms.TextBox textBoxDikeySensor;
        private System.Windows.Forms.Label labelYataySensor;
        private System.Windows.Forms.TextBox textBoxYataySensor;
        private System.Windows.Forms.Label labelYatayKontrol;
        private System.Windows.Forms.TextBox textBoxYatayKontroL;
        private System.Windows.Forms.Label labelYatayHedef;
        private System.Windows.Forms.TextBox textBoxYatayHedef;
        private System.Windows.Forms.Button buttonBaglantiKes;
        private System.Windows.Forms.Label labelPortSecim;
        private System.Windows.Forms.ComboBox comboBoxPort;
        private System.Windows.Forms.ComboBox comboBoxBaudrate;
        private System.Windows.Forms.Label labelBaudrateSecim;
        private System.Windows.Forms.Label labelDikeyBaslik;
        private System.Windows.Forms.Label labelYatayBaslik;
        private System.Windows.Forms.Button buttonPidBasla;
        private System.Windows.Forms.Button buttonPidDur;
        private System.Windows.Forms.Label labelPidDurumBaslik;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox checkBoxDenetim;
        private System.Windows.Forms.Button buttonDenetimButonu;
        private System.Windows.Forms.GroupBox groupBoxKatsayiAyari;
        private System.Windows.Forms.TrackBar trackBarDikeyKp;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label LabelPidDurumu;
        private System.Windows.Forms.TextBox textBoxDikeyKp;
        private System.Windows.Forms.TextBox textBoxYataySet;
        private System.Windows.Forms.TextBox textBoxYatayKd;
        private System.Windows.Forms.TextBox textBoxYatayKi;
        private System.Windows.Forms.TextBox textBoxYatayKp;
        private System.Windows.Forms.TextBox textBoxDikeySet;
        private System.Windows.Forms.TextBox textBoxDikeyKd;
        private System.Windows.Forms.TextBox textBoxDikeyKi;
        private System.Windows.Forms.TrackBar trackBarYataySet;
        private System.Windows.Forms.TrackBar trackBarYatayKd;
        private System.Windows.Forms.TrackBar trackBarYatayKi;
        private System.Windows.Forms.TrackBar trackBarYatayKp;
        private System.Windows.Forms.TrackBar trackBarDikeySet;
        private System.Windows.Forms.TrackBar trackBarDikeyKd;
        private System.Windows.Forms.TrackBar trackBarDikeyKi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button buttonPrePidDurumu;
    }
}

