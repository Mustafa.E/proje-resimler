﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;

namespace PidProjesi_a1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBoxBaudrate.Items.AddRange(new string[] { "9600", "115200" });
        }
        private string data;
        string Dikeyhedef;
        string Yatayhedef;
        int DikeyKp;
        int DikeyKi;
        int DikeyKd;
        int YatayKp;
        int YatayKi;
        int YatayKd;
        int DikeySet;
        int YataySet;
        string PidDurumu;

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                comboBoxPort.Items.Add(port);
            }
            comboBoxPort.SelectedIndex = 0;
            comboBoxBaudrate.SelectedIndex = 0;

            chart1.ChartAreas[0].AxisY.Minimum = -100;
            chart1.ChartAreas[0].AxisY.Maximum = 220;
            chart1.ChartAreas[0].AxisY.Interval = 10;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart1.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm:ss";

            chart2.ChartAreas[0].AxisY.Minimum = -100;
            chart2.ChartAreas[0].AxisY.Maximum = 270;
            chart2.ChartAreas[0].AxisY.Interval = 10;
            chart2.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            chart2.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart2.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm:ss";

            serialPort1.DataReceived += new SerialDataReceivedEventHandler(SerialPort1_DataReceived);
            MessageBox.Show($"Bu arayüz 2DOF Helikopter Pid Kontrol Projesi kapsamında Mustafa Ö. tarafından hazırlanmıştır.", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            buttonBaglantiKes.Enabled = false;
            buttonPidBasla.Enabled = false;
            buttonPidDur.Enabled = false;
            buttonPrePidDurumu.Enabled = false; //en son burasi yorumdan kaldirilacak.
            buttonDenetimButonu.Enabled = false;
            checkBoxDenetim.Enabled = false;
            


            LabelPidDurumu.ForeColor = Color.Red;
            LabelPidDurumu.Text = "Pid şu anda çalışmıyor..";



        }

        private void buttonBaglan_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.PortName = comboBoxPort.Text;
                serialPort1.BaudRate = Convert.ToInt32(comboBoxBaudrate.Text);
                serialPort1.Parity = Parity.None;
                serialPort1.StopBits = StopBits.One;
                serialPort1.DataBits = 8;
                serialPort1.Open();

                buttonBaglan.Enabled = false;
                buttonBaglantiKes.Enabled = true;
                //buttonPidBasla.Enabled = true;
                buttonPrePidDurumu.Enabled = true;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ("hata:"));

            }
        }

        private void buttonBaglantiKes_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.Write("0");
                serialPort1.Close();


                buttonBaglan.Enabled = true;
                buttonBaglantiKes.Enabled = false;
                buttonPidBasla.Enabled = false;
                buttonPidDur.Enabled = false;



            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, ("hata:"));
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (serialPort1.IsOpen == true)
            {
                serialPort1.Close();
            }
        }

        private void SerialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            data = serialPort1.ReadLine();
            this.BeginInvoke(new EventHandler(Display_data));
        }

        private void Display_data(object sender, EventArgs e)
        {
            DateTime myDateValue = DateTime.Now;
            //string[] value = data.Split('/');

            try
            {
                sbyte indexOfK = Convert.ToSByte(data.IndexOf("K"));
                sbyte indexOfL = Convert.ToSByte(data.IndexOf("L"));
                sbyte indexOfM = Convert.ToSByte(data.IndexOf("M"));
                sbyte indexOfN = Convert.ToSByte(data.IndexOf("N"));

                string deger1 = data.Substring(0, indexOfK);
                string deger2 = data.Substring(indexOfK + 1, (indexOfL - indexOfK) - 1);
                string deger3 = data.Substring(indexOfL + 1, (indexOfM - indexOfL) - 1);
                string deger4 = data.Substring(indexOfM + 1, (indexOfN - indexOfM) - 1);

                textBoxDikeyKontroL.Text = deger1;
                textBoxDikeySensor.Text = deger2;
                textBoxYatayKontroL.Text = deger3;
                textBoxYataySensor.Text = deger4;

                textBoxDikeyHedef.Text = Convert.ToString(DikeySet);
                textBoxYatayHedef.Text = Convert.ToString(YataySet);

                double t = Convert.ToDouble(deger1);
                double g = Convert.ToDouble(deger2);
                double h = Convert.ToDouble(deger3);
                double r = Convert.ToDouble(deger4);

                double k = Convert.ToDouble(DikeySet);
                double m = Convert.ToDouble(YataySet);


                this.chart1.Series[0].Points.AddXY(myDateValue.ToString("HH:mm:ss"), t);
                this.chart1.Series[1].Points.AddXY(myDateValue.ToString("HH:mm:ss"), g);
                this.chart1.Series[2].Points.AddXY(myDateValue.ToString("HH:mm:ss"), k);

                this.chart2.Series[0].Points.AddXY(myDateValue.ToString("HH:mm:ss"), h);
                this.chart2.Series[1].Points.AddXY(myDateValue.ToString("HH:mm:ss"), r);
                this.chart2.Series[2].Points.AddXY(myDateValue.ToString("HH:mm:ss"), m);


             

              

            }
            catch (Exception hata)
            {

                MessageBox.Show(hata.Message);
            }



        }

        private void buttonPidBasla_Click(object sender, EventArgs e)
        {
            LabelPidDurumu.ForeColor = Color.Green;
            LabelPidDurumu.Text = "Pid şu anda çalışıyor..";

            PidDurumu = "1";

            if (serialPort1.IsOpen)
            {
                if (!checkBoxDenetim.Checked)
                {
                    serialPort1.Write(PidDurumu + "T" + "\n");
                }
            }


            buttonPidBasla.Enabled = false;
            buttonPidDur.Enabled = true;
            buttonPrePidDurumu.Enabled = false;
            checkBoxDenetim.Enabled = true;
            // groupBoxKatsayiAyari.Enabled = true;

        }

        private void buttonPidDur_Click(object sender, EventArgs e)
        {
            LabelPidDurumu.ForeColor = Color.Red;
            LabelPidDurumu.Text = "Pid şu anda çalışmıyor..";

            PidDurumu = "0";

            if (serialPort1.IsOpen)
            {
                if (!checkBoxDenetim.Checked)
                {
                    serialPort1.Write(PidDurumu + "T" + "\n");
                }
            }

            
            buttonPidDur.Enabled = false;
            buttonPrePidDurumu.Enabled = true;
            checkBoxDenetim.Enabled = false;
            buttonDenetimButonu.Enabled = false;


        }

        private void trackBarDikeyKp_Scroll(object sender, EventArgs e)
        {
            DikeyKp = trackBarDikeyKp.Value;

            
            textBoxDikeyKp.Text = DikeyKp.ToString();

            if(PidDurumu == "1")
            {
                if (serialPort1.IsOpen)
                {
                    if (!checkBoxDenetim.Checked)
                    {
                        serialPort1.Write(DikeyKp + "A" + "\n");
                    }
                }
            }
        }
        
        private void trackBarDikeyKi_Scroll(object sender, EventArgs e)
        {
            DikeyKi = trackBarDikeyKi.Value;


            textBoxDikeyKi.Text = DikeyKi.ToString();

            if (PidDurumu == "1")
            {
                if (serialPort1.IsOpen)
                {
                    if (!checkBoxDenetim.Checked)
                    {
                        serialPort1.Write(DikeyKi + "B" + "\n");
                    }
                }
            }  
        }

        private void trackBarDikeyKd_Scroll(object sender, EventArgs e)
        {
            DikeyKd = trackBarDikeyKd.Value;


            textBoxDikeyKd.Text = DikeyKd.ToString();

            if (PidDurumu == "1")
            {
                if (serialPort1.IsOpen)
                {
                    if (!checkBoxDenetim.Checked)
                    {
                        serialPort1.Write(DikeyKd + "C" + "\n");
                    }
                }
            }
        }

        private void trackBarDikeySet_Scroll(object sender, EventArgs e)
        {
            DikeySet = trackBarDikeySet.Value;


            textBoxDikeySet.Text = DikeySet.ToString();

            if (PidDurumu == "1")
            {
                if (serialPort1.IsOpen)
                {
                    if (!checkBoxDenetim.Checked)
                    {
                        serialPort1.Write(DikeySet + "G" + "\n");
                    }
                }
            }
        }

        private void trackBarYatayKp_Scroll(object sender, EventArgs e)
        {
            YatayKp = trackBarYatayKp.Value;


            textBoxYatayKp.Text = YatayKp.ToString();

            if (PidDurumu == "1")
            {
                if (serialPort1.IsOpen)
                {
                    if (!checkBoxDenetim.Checked)
                    {
                        serialPort1.Write(YatayKp + "D" + "\n");
                    }
                }
            }  
        }

        private void trackBarYatayKi_Scroll(object sender, EventArgs e)
        {
            YatayKi = trackBarYatayKi.Value;


            textBoxYatayKi.Text = YatayKi.ToString();

            if (PidDurumu == "1")
            {
                if (serialPort1.IsOpen)
                {
                    if (!checkBoxDenetim.Checked)
                    {
                        serialPort1.Write(YatayKi + "E" + "\n");
                    }
                }
            } 
        }

        private void trackBarYatayKd_Scroll(object sender, EventArgs e)
        {
            YatayKd = trackBarYatayKd.Value;


            textBoxYatayKd.Text = YatayKd.ToString();

            if (PidDurumu == "1")
            {
                if (serialPort1.IsOpen)
                {
                    if (!checkBoxDenetim.Checked)
                    {
                        serialPort1.Write(YatayKd + "F" + "\n");
                    }
                }
            }   
        }

        private void trackBarYataySet_Scroll(object sender, EventArgs e)
        {
            YataySet = trackBarYataySet.Value;


            textBoxYataySet.Text = YataySet.ToString();

            if (PidDurumu == "1")
            {
                if (serialPort1.IsOpen)
                {
                    if (!checkBoxDenetim.Checked)
                    {
                        serialPort1.Write(YataySet + "H" + "\n");
                    }
                }
            } 
        }
        private void checkBoxDenetim_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxDenetim.Checked)
            {
                buttonDenetimButonu.Enabled = true;
            }
            else
            {
                buttonDenetimButonu.Enabled = false;
            }
        }

        private void buttonDenetimButonu_Click(object sender, EventArgs e)
        {

            if (PidDurumu == "1")
            {
                if (serialPort1.IsOpen)
                {
                    DikeyKp = trackBarDikeyKp.Value;
                    DikeyKi = trackBarDikeyKi.Value;
                    DikeyKd = trackBarDikeyKd.Value;
                    DikeySet = trackBarDikeySet.Value;
                    YatayKp = trackBarYatayKp.Value;
                    YatayKi = trackBarYatayKi.Value;
                    YatayKd = trackBarYatayKd.Value;
                    YataySet = trackBarYataySet.Value;

                    serialPort1.Write(DikeyKp + "A" + DikeyKi + "B" + DikeyKd + "C" + YatayKp + "D"
                        + YatayKi + "E" + YatayKd + "F" + DikeySet + "G" + YataySet + "H"
                        + PidDurumu + "T" + "\n");
                }
            }  
        }

        private void buttonPrePidDurumu_Click(object sender, EventArgs e)
        {
            buttonPidBasla.Enabled = true;
            buttonPrePidDurumu.Enabled = false;
            buttonPidDur.Enabled = false;
        }
    }
}
